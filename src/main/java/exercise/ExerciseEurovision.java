package exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"business", "rest"})
@EnableJpaRepositories(basePackages = "repository")
@EntityScan("entities")
public class ExerciseEurovision {
    public static void main(String[] args) {
        SpringApplication.run(ExerciseEurovision.class, args);
    }
}
