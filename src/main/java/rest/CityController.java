package rest;

import business.CityService;
import entities.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import rest.reponse.CitiesResponse;

import java.util.List;

@Controller
public class CityController {

    @Autowired
    private CityService cityService;

    /**
     * Returns a JSON with the city with the corresponding ID
     * @param id id of the city
     * @return City with the corresponding ID
     */
    @GetMapping(path = "api/cities/{id}", produces = "application/json")
    public City findById(@PathVariable("id") Integer id){
        return cityService.findById(id);
    }
    /**
     * Returns a JSON with all the cities available
     * @return All cities available
     */
    @GetMapping(path = "api/cities", produces = "application/json")
    public List<City> findAll() {
        return cityService.findAll();
    }

    /**
     * Returns a JSON with the cities paged, showing the selected page number, how many items are shown by page, the total number of pages and the total number of cities
     * @param pageNumber page number selected
     * @param itemsByPage number of cities by page
     * @return Cities in paged selected depending on the number of cities by page
     */
    @GetMapping(path = "api/cities/queryByPage", produces = "application/json")
    public CitiesResponse findAllByPage(@RequestParam(name = "page") Integer pageNumber, @RequestParam(name = "size") Integer itemsByPage) {
        return cityService.findAllByPage(pageNumber, itemsByPage);
    }

    /**
     * Returns a JSON with the biggest sequence of cities based on City.Id, ordered alphabetical.
     * @return Biggest sequence
     */
    @GetMapping(path = "api/cities/biggest_sequence/{limit}", produces = "application/json")
    public List<City> findBiggestSequence(@PathVariable("limit") Integer limit){
        return cityService.findBiggestSequence(limit);
    }
}
