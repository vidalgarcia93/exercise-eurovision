package rest.reponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.type.TypeReference;
import entities.City;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents JSON of the response in the Controller
 */
public class CitiesResponse {

    /**
     * Cities in the page selected depending on the page size
     */
    private List<City> content;
    /**
     * Total number of pages generated
     */
    private int totalPages;
    /**
     * Total number of elements
     */
    private long totalElements;
    /**
     * Shows if the page returned is the last page
     */
    private boolean last;
    /**
     * Size of the page
     */
    private int size;
    /**
     * Page number
     */
    private int number;

    public CitiesResponse(){}

    /**
     * Constructor by the Page value returned by Service
     * @param pageCities
     */
    public CitiesResponse(Page<City> pageCities){
        content = pageCities.getContent();
        totalPages = pageCities.getTotalPages();
        totalElements = pageCities.getTotalElements();
        size = pageCities.getSize();
        number = pageCities.getNumber();
        last = pageCities.isLast();
    }

    public List<City> getContent() {
        return content;
    }

    public void setContent(List<City> content) {
        this.content = content;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CitiesResponse that = (CitiesResponse) o;
        return getTotalPages() == that.getTotalPages() &&
                getTotalElements() == that.getTotalElements() &&
                isLast() == that.isLast() &&
                getSize() == that.getSize() &&
                getNumber() == that.getNumber() &&
                Objects.equals(getContent(), that.getContent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getContent(), getTotalPages(), getTotalElements(), isLast(), getSize(), getNumber());
    }
}
