package repository;

import entities.City;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends PagingAndSortingRepository<City, Integer> {

    public List<City> findByOrderByNameAsc(Pageable pageable);
}
