package business;

import entities.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import repository.CityRepository;
import rest.reponse.CitiesResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class CityService {
    @Autowired
    private CityRepository cityRepository;

    /**
     * returns a city
     * @param id Id of City
     * @return City
     */
    public City findById(Integer id) {
        return cityRepository.findById(id).get();
    }

    /**
     * returns all the cities available
     * @return list all cities
     */
    public List<City> findAll() {
        List<City> cityList = new ArrayList<>();
        cityRepository.findAll().forEach(cityList::add);
        return cityList;
    }

    /**
     * returns the cities shown in the page selected depending on the size of page
     * @param page number of page selected
     * @param size number of cities shown in each page
     * @return cities response
     */
    public CitiesResponse findAllByPage(Integer page, Integer size){
        Pageable pageable  = PageRequest.of(page, size);
        Page<City> p = cityRepository.findAll(pageable);
        return new CitiesResponse(p);
    }

    /**
     * returns the biggest sequence based on the City.Id ascendant, adjacent or not
     * @return list of the biggest sequence
     */
    public List<City> findBiggestSequence(int limit){
        Pageable p = PageRequest.of(0, limit);
        List<City> listOrder = cityRepository.findByOrderByNameAsc(p);
        return getListOrder(listOrder, 0);
    }

    /**
     * Recursive method to find the biggest sequence. For each element of the list, it searches
     * the biggest subsequence based on City.Id, reducing the list until one element.
     * @param findList List of elements to search
     * @param id of the lowest City from which to search for the following cities
     * @return biggest subsequence
     */
    private List<City> getListOrder(List<City> findList, Integer id){
        List<City> biggest = new ArrayList<>();
        for(City c: findList){
            List<City> actual = new ArrayList<>();
            if (c.getId() > id) {
                actual.add(c);
                actual.addAll(getListOrder(findList.subList(findList.indexOf(c)+1, findList.size()), c.getId()));
                if (biggest.size() <= actual.size()) {
                    biggest = actual;
                }
            }
        }
        return biggest;
    }
}
