package rest;

import business.CityService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import entities.City;
import exercise.ExerciseEurovision;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import repository.CityRepository;
import rest.reponse.CitiesResponse;
import utils.HelperPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = ExerciseEurovision.class)
public class CityControllerTest {

    private static MockMvc mockMvc = null;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    private CityController cityController;

    @MockBean
    private CityRepository cityRepository;

    @Before
    public void initialize() {
        if (mockMvc == null) {
            mockMvc = webAppContextSetup(webApplicationContext).build();
        }

        ObjectMapper mapper = new ObjectMapper();
        List<City> cities = new ArrayList<>();
        List<City> allCities = new ArrayList<>();
        City one = new City();
        Page<City> citiesResponse = new PageImpl<>(new ArrayList<>());
        try {
            cities = mapper.readValue(new File("src/test/resources/cities.json"), new TypeReference<List<City>>() {
            });
            allCities = mapper.readValue(new File("src/test/resources/allcities.json"), new TypeReference<List<City>>() {
            });
            one = mapper.readValue(new File("src/test/resources/onecity.json"), City.class);
            citiesResponse = mapper.readValue(new File("src/test/resources/citiesresponse.json"), new TypeReference<HelperPage<City>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            fail();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Pageable pageableLimit = PageRequest.of(0, 30);
        when(cityRepository.findByOrderByNameAsc(pageableLimit)).thenReturn(cities);
        when(cityRepository.findAll()).thenReturn(allCities);
        when(cityRepository.findById(223)).thenReturn(java.util.Optional.ofNullable(one));
        Pageable pageable = PageRequest.of(0, 5);
        when(cityRepository.findAll(pageable)).thenReturn(citiesResponse);
    }

    /**
     * Test findById with controller rest
     */
    @Test
    public void testFindById() {
        try {
            MvcResult result = mockMvc.perform(get("/api/cities/223")).andExpect(status().is2xxSuccessful()).andReturn();
            ObjectMapper mapper = new ObjectMapper();
            City c = (City) result.getModelAndView().getModel().get("city");
            City cExpected = mapper.readValue(new File("src/test/resources/onecity.json"), City.class);
            assertEquals(cExpected, c);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test findAll with controller rest
     */
    @Test
    public void testFindAll() {
        try {
            MvcResult result = mockMvc.perform(get("/api/cities")).andExpect(status().is2xxSuccessful()).andReturn();
            ObjectMapper mapper = new ObjectMapper();
            List<City> c = (List<City>) result.getModelAndView().getModel().get("cityList");
            List<City> cExpected = mapper.readValue(new File("src/test/resources/allcities.json"), new TypeReference<List<City>>() {
            });
            assertEquals(cExpected, c);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test findAllByPage with controller rest
     */
    @Test
    public void testFindAllByPage() {
        try {
            MvcResult result = mockMvc.perform(get("/api/cities/queryByPage?page=0&size=5")).andExpect(status().is2xxSuccessful()).andReturn();
            ObjectMapper mapper = new ObjectMapper();
            CitiesResponse c = (CitiesResponse) result.getModelAndView().getModel().get("citiesResponse");
            CitiesResponse cExpected = mapper.readValue(new File("src/test/resources/responseExpected.json"), CitiesResponse.class);
            assertEquals(cExpected, c);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test findBiggestSequence with controller rest
     */
    @Test
    public void testFindBiggestSequence() {
        try {
            MvcResult result = mockMvc.perform(get("/api/cities/biggest_sequence/30")).andExpect(status().is2xxSuccessful()).andReturn();
            ObjectMapper mapper = new ObjectMapper();
            List<City> c = (List<City>) result.getModelAndView().getModel().get("cityList");
            List<City> cExpected = mapper.readValue(new File("src/test/resources/citiesExpected.json"), new TypeReference<List<City>>() {
            });
            assertEquals(cExpected, c);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}
