package business;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import entities.City;
import exercise.ExerciseEurovision;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import repository.CityRepository;
import rest.reponse.CitiesResponse;
import utils.HelperPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = ExerciseEurovision.class)
public class CityServiceTest {

    @Autowired
    private CityService cityService;

    @MockBean
    private CityRepository cityRepository;

    /**
     * Function to load the differents data that tests are waiting for.
     */
    @Before
    public void loadCities() {
        ObjectMapper mapper = new ObjectMapper();
        List<City> cities = new ArrayList<>();
        List<City> allCities = new ArrayList<>();
        City one = new City();
        Page<City> citiesResponse = new PageImpl<>(new ArrayList<>());
        try {
            cities = mapper.readValue(new File("src/test/resources/cities.json"), new TypeReference<List<City>>() {
            });
            allCities = mapper.readValue(new File("src/test/resources/allcities.json"), new TypeReference<List<City>>() {
            });
            one = mapper.readValue(new File("src/test/resources/onecity.json"), City.class);
            citiesResponse = mapper.readValue(new File("src/test/resources/citiesresponse.json"), new TypeReference<HelperPage<City>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            fail();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Pageable pageableLimit = PageRequest.of(0, 30);
        when(cityRepository.findByOrderByNameAsc(pageableLimit)).thenReturn(cities);
        when(cityRepository.findAll()).thenReturn(allCities);
        when(cityRepository.findById(223)).thenReturn(java.util.Optional.ofNullable(one));
        Pageable pageable  = PageRequest.of(0, 5);
        when(cityRepository.findAll(pageable)).thenReturn(citiesResponse);
    }

    /**
     * Test the findById function
     */
    @Test
    public void testFindById(){
        City city = cityService.findById(223);
        ObjectMapper mapper = new ObjectMapper();
        try {
            City cityExpected = mapper.readValue(new File("src/test/resources/onecity.json"), City.class);
            assertEquals(cityExpected, city);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test the findAll function
     */
    @Test
    public void testFindAll(){
        List<City> cities = cityService.findAll();
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<City> expectedCities = mapper.readValue(new File("src/test/resources/allcities.json"), new TypeReference<List<City>>() {
            });
            Assert.assertEquals(expectedCities, cities);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test the findAll function listed by pages
     */
    @Test
    public void testFindAllByPage(){
        CitiesResponse response = cityService.findAllByPage(0, 5);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("src/test/resources/responseExpected.json"), response);
            CitiesResponse expectedResponse = mapper.readValue(new File("src/test/resources/responseExpected.json"), CitiesResponse.class);
            Assert.assertEquals(expectedResponse, response);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test the findBiggestSequence function of CityService
     */
    @Test
    public void testFindBiggestSequence() {
        List<City> cities = cityService.findBiggestSequence(30);
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<City> expectedCities = mapper.readValue(new File("src/test/resources/citiesExpected.json"), new TypeReference<List<City>>() {
            });
            Assert.assertEquals(expectedCities, cities);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }
}
